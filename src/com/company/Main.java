package com.company;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        int dado[] = {1, 2, 3, 4, 5, 6};
        int soma = 0;

        for(int i = 0; i < 3; i++) {
            int numDado = random.nextInt(dado.length);
            int numeroSorteado = dado[numDado];
            System.out.println("Número sorteado: " + numeroSorteado);
            soma = soma + numeroSorteado;
        }

            System.out.println("Soma dos números sorteados: " + soma);
    }
}
